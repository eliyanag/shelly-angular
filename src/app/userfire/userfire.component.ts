import { Component, OnInit } from '@angular/core';
import { UsersService } from './../users/users.service';
import { Router } from "@angular/router"; //Login without JWT

@Component({
  selector: 'userfire',
  templateUrl: './userfire.component.html',
  styleUrls: ['./userfire.component.css']
})
export class UserfireComponent implements OnInit {

  users;
  //Login without JWT router
  constructor(private service:UsersService, private router:Router) { }

  ngOnInit() {
    this.service.getUsersFire().subscribe(response=>{
        console.log(response);
        this.users = response;
    });
    //Login without JWT
    var value = localStorage.getItem('auth');
    
    if(value == 'true'){   
     // this.router.navigate(['/']);
    }else{
      this.router.navigate(['/login']);
    }  
  }

}
