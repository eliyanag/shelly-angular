import { Injectable } from '@angular/core';
//Q2
import { Http , Headers } from '@angular/http';
import { HttpParams } from '@angular/common/http';
//Q5
import { AngularFireDatabase } from 'angularfire2/database';
//Deployment
import { environment } from './../../environments/environment';

@Injectable()
export class UsersService {
  http:Http; //Q2
  
  //Q5
   getUsersFire(){
    return this.db.list('/users').valueChanges();
  }
  
  //Q2
  getUsers(){
    return this.http.get(environment.url+'users');
  }
  //Q3
  postUsers(data){
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    //בגרש נגדיר את השדות בהתאם לערכים שהגדרנו בשרת
    let params = new HttpParams().append('name',data.name).append('phonenumber',data.phonenumber);
    return this.http.post(environment.url+'users', params.toString(), options);
  }
  //Q4 1
  deleteUser(key){
    return this.http.delete(environment.url+'users/'+ key);
  }

  //Q4 2
  putUser(data,key){
    let options = {
      headers: new Headers({
       'content-type':'application/x-www-form-urlencoded'
      })
    }
    //בגרש נגדיר את השדות בהתאם לערכים שהגדרנו בשרת
    let params = new HttpParams().append('name',data.name).append('phonenumber',data.phonenumber);
    return this.http.put(environment.url+'users/'+ key,params.toString(), options);
  }

  getUser(id){
     return this.http.get(environment.url+'users/'+ id);
  }

  //Login Witout JWT
  login(credentials){
     let options = {
        headers:new Headers({
         'content-type':'application/x-www-form-urlencoded'
        })
     }
    let  params = new HttpParams().append('name', credentials.user).append('password',credentials.password);
    return this.http.post(environment.url+'login', params.toString(),options).map(response=>{ 
      let success = response.json().success;
      if (success == true){
        localStorage.setItem('auth','true');
      }else{
        localStorage.setItem('auth','false');        
      }
   });
  }

  constructor(http:Http, private db:AngularFireDatabase) { 
    this.http = http;
  }

}
